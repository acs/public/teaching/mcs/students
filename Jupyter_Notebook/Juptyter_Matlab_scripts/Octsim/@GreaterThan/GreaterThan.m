classdef GreaterThan < Block
    properties
     tr
     fa
    end
    methods
        function c = GreaterThan(in1,in2,out,tr,fa)
            c = c@Block();
            c.ninput = 2;
            c.inpos(1) = in1;
            c.inpos(2) = in2;           
            c.noutput = 1;
            c.outpos(1) = out;
            c.tr = tr;
            c.fa = fa;
        end 
        
        function flag = Step(c,t,dt)
            if c.u(1) > c.u(2)
                c.y = c.tr;
            else
                c.y = c.fa;
            end
            flag = 1;
        end
        
    end
end
            
            
            
        
