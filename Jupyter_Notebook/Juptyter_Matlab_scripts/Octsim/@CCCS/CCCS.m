classdef CCCS < RCComponent
    properties
        cpos  
        cneg
        gain
    end
    methods
        function v = CCCS(p,n,cp,cn,g)
            v = v@RCComponent(p,n);
            v.gain = g;
            v.cpos = cp;
            v.cneg = cn;
           
        end   
        
        function ApplyMatrixStamp(v,P,dt)
            ntot = size(P.G,1);
            v.possource = ntot+1;
            P.G =  [P.G,zeros(ntot,1)];
            P.G =  [P.G; zeros(1,ntot+1)];
            P.b=   [P.b; 0];  
            if (v.Pos > 0)
                P.G(b.Pos, v.possource) = v.g;          
            end
            if (v.Neg > 0)    
                P.G(v.Neg,v.possource) = -v.g;
            end
            if(v.cpos > 0)
                    P.G(v.possource,v.cpos)=+1;
                    P.G(v.cpos,v.possource)=+1;
            end
            if(v.cneg > 0)
                    P.G(v.possource,v.cneg)=-1;
                    P.G(v.cneg,v.possource)=-1;
            end
            
        end
        
        function Init(v,P,dt)
            v.ApplyMatrixStamp(P,dt);         
        end
        
        function Step(v,P,dt,t)
        end
        
        function PostStep(v,vsol,dt)
        end

    end
end
            
            
            
        
