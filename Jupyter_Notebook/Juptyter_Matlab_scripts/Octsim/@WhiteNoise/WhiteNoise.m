classdef WhiteNoise < Block
    properties
        Sv
    end
    methods
        function c = WhiteNoise(out,Sv)
            c = c@Block();
            c.Sv = Sv;
            c.noutput = 1;
            c.outpos = out;
        end 
        
        function flag = Step(c,t,dt)
            c.y = normrnd(0,c.Sv,1); 
            flag = 1;
        end
        
        function flag = ChangeParameters(c,value)
            c.Sv = value;
        end        
        
    end
end
        
            
            
        
