classdef SawTooth < Block
    properties
        hi
        lo
        f
        Ts
    end
    methods
        function c = SawTooth(out,hi,lo,f)
            c = c@Block();
            c.hi = hi;
            c.lo = lo;     
            c.f = f;
            c.Ts = 1/f;
            c.noutput = 1;
            c.outpos(1) = out;
        end 
        
        function flag = Step(c,t,dt)
            d = t/c.Ts-floor(t/c.Ts);
            c.y = c.lo + d*(c.hi-c.lo);
            flag = 1;
        end
        
    end
end
            
            
            
        
