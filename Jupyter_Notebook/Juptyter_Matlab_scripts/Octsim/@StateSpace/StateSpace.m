classdef StateSpace < Block
    properties
        A
        B
        C
        D
        xo
    end
    methods
        function c = StateSpace(in,out,A,B,C,D,xo)
            c = c@Block();
            c.A = A;
            c.B = B;
            c.C = C;
            c.D = D;
            c.ninput = size(B,2);
            c.noutput = size(C,1);
            c.inpos = in;
            c.outpos = out;
            c.nstate = size(c.A,1);
            c.x = xo;
            c.xo = xo;
        end 
        
        function flag = Step(c,t,dt)
            c.y = c.C*c.x+c.D*c.u';
            c.updatestate(t,dt);      
            flag = 1;
        end
        
        function flag = Reset(b)
            b.x = b.xo;
            flag = 1;
        end
        
        function [dxdt] = cdxdt(c,x,t)
            dxdt = c.A*x+c.B*c.u';
        end
        
        
    end
end
            
            
            
        
