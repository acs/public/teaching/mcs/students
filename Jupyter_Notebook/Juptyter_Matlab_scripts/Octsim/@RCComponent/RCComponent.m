classdef RCComponent < handle
    properties
        Pos;
        Neg;
	linear = 1; 
    hyin = 0;
    hyout = 0;
    end
    
    methods
        function rc = RCComponent(p,n)
            rc.Pos = p;
            rc.Neg = n;
        end
        
        function ApplyMatrixStamp(P,dt)
        end
        
        function Init(P,dt)
        end
        
        function Step(P,dt,t)
        end
        
        function PostStep(P,dt)
        end
        
    end
    
end
            
            
            
        
