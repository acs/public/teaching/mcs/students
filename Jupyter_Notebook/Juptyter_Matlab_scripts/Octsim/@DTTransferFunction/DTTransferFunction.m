classdef DTTransferFunction < DTBlock
    properties
        #A
        #B
        #C
        #D
        num
        den
        nn
        bufn
        bufd
    end
    methods
        function c = DTTransferFunction(in,out,num,den,Ts)
            c = c@DTBlock();
            c.ninput = 1;
            c.noutput = 1;
            c.inpos(1) = in;
            c.outpos(1) = out;
	        c.Ts = Ts;
            c.num = num;
            c.den = den;
            sys = tf(num,den,Ts);
            sysss= ss(sys);
            #c.A = sysss.A;
            #c.B = sysss.B;
            #c.C = sysss.C;
            #c.D = sysss.D;
            #c.nstate = size(c.A,1);
            #c.x = zeros(c.nstate,1);
            c.nn = size(c.den,2);
            nt = size(c.num,2);
            c.bufd = zeros(c.nn,1);
            c.bufn = zeros(c.nn,1);
            dif = c.nn - nt;
            c.num = [zeros(1,dif) c.num];
        end 
        
       function updatediscrete(c)
          #c.y = c.C*c.x+c.D*c.u';
          #c.x=c.A*c.x+c.B*c.u';      
          for i=c.nn:-1:2
                c.bufn(i) = c.bufn(i-1);
          end
          c.bufn(1) = c.u;    
          for i=c.nn:-1:2
                c.bufd(i) = c.bufd(i-1);
          end
          c.bufd(1) = (c.num*c.bufn-c.den(2:c.nn)*c.bufd(2:c.nn))/c.den(1);    
          c.y = c.bufd(1);   
          flag = 1;
       end
       
       function flag = Reset(b)
           #b.x = zeros(c.nstate,1);
           b.bufn = zeros(b.nn,1);
           b.bufd = zeros(b.nn,1);
           flag = 1;
        end
             
    end
end
            
            
            
        
