classdef ACCurrentSource < RCComponent
    properties
        Amp
        Fre
        Ph
    end
    methods
        function b = ACCurrentSource(p,n,Amp,Fre,Ph)
            b = b@RCComponent(p,n);
            b.Amp = Amp;
            b.Fre = Fre;
            b.Ph = Ph;
        end
        
        function ApplyMatrixStamp(b,P,dt)
          
        end
        
        function flag = ChangeParameters(b,value)
            b.Amp = value(1);
            b.Fre = value(2);
            b.Ph = value(3);
            flag = 1;
        end

        
        function Init(b,P,dt)
            b.ApplyMatrixStamp(P,dt);         
        end
        
        function Step(b,P,dt,t)
         val = b.Amp*cos(2*pi*b.Fre*t+b.Ph)
         if (b.Pos > 0)
               P.b(b.Pos)= P.b(b.Pos)+val;
         end
        if (b.Neg > 0)
               P.b(b.Neg)= P.b(b.Neg)-val;          
        end
        
        end
        
        function PostStep(b,vsol,dt)
        end

    end
end
            
            
            
        
