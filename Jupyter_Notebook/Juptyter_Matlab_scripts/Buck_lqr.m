clc;clear;
L = 1e-3;
r = 0.1;
C_o = 1e-3;
R_L = 10;
V_dc = 500;

A = [-r/L -1/L; 1/C_o -1/(R_L*C_o)]
B = [V_dc/L; 0]
C = [0 1]
D = [0]

sys_cont = ss(A,B,C,D)
Ts = 1e-5; % Simulation time step
sys_disc = c2d(sys_cont,Ts)
Ql = [1e9 0;0 1]
Rl = 1e3;
K = dlqr(sys_disc.A,sys_disc.B,Ql,Rl)
x = [20; 200];
T_sim = 0.2; % Simulation time
npoints = floor(T_sim/Ts) % total number of simulation time stamps
y_ref = 200;
setpoint = ([sys_disc.A - eye(size(A,1)) sys_disc.B; C 0])\[zeros(size(sys_disc.A,1),1);y_ref]
x_ref = setpoint(1:2,1);
u_ref = setpoint(3,1)
for i=1:npoints
    t(i,1) = (i-1)*Ts; % time stamp
    
    if t(i,1) > T_sim/2
        y_ref = 350;
    end
    
    
    setpoint = ([sys_disc.A - eye(size(A,1)) sys_disc.B; C 0])\[zeros(size(sys_disc.A,1),1);y_ref];
    x_ref = setpoint(1:2,1);
    u_ref = setpoint(3,1);
    
    u(i,1) = -K*(x(:,i)-x_ref) + u_ref;
    if(u(i,1) < 0)
        u(i,1) = 0;
    end
    if(u(i,1) > 1)
        u(i,1) = 1;
    end
    x(:,i+1)= sys_disc.A*x(:,i) + sys_disc.B*u(i,1);
end
t(i+1,1) = npoints*Ts;
u(i+1) = -K*x(:,i+1);
%u(i+1)
figure
plot(t,x(1,:),'b')
figure
plot(t,x(2,:),'b')
figure
plot(t,u)