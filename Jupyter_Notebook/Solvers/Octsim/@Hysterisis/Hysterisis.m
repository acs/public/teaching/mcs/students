classdef Hysterisis < Block
    properties
    th
    hi
    lo
    status
    end
    methods
        function c = Hysterisis(in,out,th,hi,lo)
            c = c@Block();
            c.ninput = 1;
            c.inpos(1) = in;
            c.noutput = 1;
            c.outpos(1) = out;
            c.th = th;
            c.hi = hi;
            c.lo = lo;
            c.status = c.hi;
        end 
        
        function flag = Step(c,t,dt)
            if c.u > c.th
                c.status = c.hi;
            elseif c.u < -c.th
                c.status = c.lo;
            end
            c.y = c.status;
            flag = 1;
        end
        
    end
end
            
            
            
        
