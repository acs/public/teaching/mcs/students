classdef RealDCVoltageSource < RCComponent
    properties
        Vs
        Rs
        As
	Gs
    end
    methods
        function b = RealDCVoltageSource(p,n,V,R)
            b = b@RCComponent(p,n);
            b.Vs = V;
            b.Rs = R;          
        end
        
        function ApplyMatrixStamp(b,P,dt)
	    if(b.Rs > 0)
    		b.Gs = 1/b.Rs;
        	b.As = b.Vs/b.Rs;
	    end
	    if(b.Pos > 0)
                    P.G(b.Pos,b.Pos)=P.G(b.Pos,b.Pos)+b.Gs;
            end
            if(b.Neg > 0)
                    P.G(b.Neg,b.Neg)=P.G(b.Neg,b.Neg)+b.Gs;
            end
            if (b.Pos>0) & (b.Neg>0)
                P.G(b.Pos,b.Neg)=P.G(b.Pos,b.Neg)-b.Gs;
                P.G(b.Neg,b.Pos)=P.G(b.Neg,b.Pos)-b.Gs;
            end       

        end
        
        function Init(b,P,dt)
            b.ApplyMatrixStamp(P,dt);         
        end
        
        function Step(b,P,dt,t)
       
         if (b.Pos > 0)
               P.b(b.Pos)= P.b(b.Pos)+b.As;
         end
        if (b.Neg > 0)
               P.b(b.Neg)= P.b(b.Neg)-b.As;          
        end
        
        end
        
        function PostStep(b,vsol,dt)
        end
    end
end
            
            
            
        
