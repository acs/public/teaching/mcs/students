classdef IIR < DTBlock
    properties
        a
        b
        n
        d
        bufn
        bufd
    end
    methods
        function c = IIR(in,out,a,b,T)
            c = c@DTBlock();
            c.a = a;
            c.b = b
            c.Ts = T;
            c.ninput = 1;
            c.noutput = 1;
            c.inpos = in;
            c.outpos = out;
            c.n = size(c.a,2);
            c.d = size(c.b,2)-1;
            c.bufn = zeros(c.n,1);
            c.bufd = zeros(c.n,1);
        end 
        
        function updatediscrete(c)
            for i=c.n:-1:2
                c.bufn(i) = c.bufn(i-1);
            end
            c.bufn(1) = c.u;    
            for i=c.d:-1:2
                c.bufd(i) = c.bufd(i-1);
            end
            c.y = c.a*c.bufn-c.b*c.bufd;      
            flag = 1;
        end
        
        function flag = Reset(b);
            b.bufn = zeros(b.n,1);
            b.bufd = zeros(b.d,1);
            flag = 1;
        end
        
    end
end
            