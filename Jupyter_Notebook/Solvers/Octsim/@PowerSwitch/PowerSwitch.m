classdef PowerSwitch < RCComponent
    properties
        Gc
        Ron 
        Roff
        flin
    end
    methods
        function r = PowerSwitch(p,n,fl,Ron,Roff)
            r = r@RCComponent(p,n);
            r.Ron = Ron;
            r.Roff = Roff;
            r.flin = fl;
            r.hyin = 1;
            r.linear = 0;
            r.Gc = 1/Ron; 
        end   
        
        function ApplyMatrixStamp(r,P,dt)
            if(r.Pos > 0)
                    P.G(r.Pos,r.Pos)=P.G(r.Pos,r.Pos)+r.Gc;
            end
            if(r.Neg > 0)
                    P.G(r.Neg,r.Neg)=P.G(r.Neg,r.Neg)+r.Gc;
            end
            if (r.Pos>0) & (r.Neg>0)
                P.G(r.Pos,r.Neg)=P.G(r.Pos,r.Neg)-r.Gc;
                P.G(r.Neg,r.Pos)=P.G(r.Neg,r.Pos)-r.Gc;
            end       
        end
        
        function ApplyMatrixStampdp(r,P,dt,om)
            if(r.Pos > 0)
                    P.G(r.Pos,r.Pos)=P.G(r.Pos,r.Pos)+r.Gc;
            end
            if(r.Neg > 0)
                    P.G(r.Neg,r.Neg)=P.G(r.Neg,r.Neg)+r.Gc;
            end
            if (r.Pos>0) & (r.Neg>0)
                P.G(r.Pos,r.Neg)=P.G(r.Pos,r.Neg)-r.Gc;
                P.G(r.Neg,r.Pos)=P.G(r.Neg,r.Pos)-r.Gc;
            end       
        end
        
        function Init(r,P,dt)
            r.ApplyMatrixStamp(P,dt);         
        end
        
        function Initdp(r,P,dt,om)
            r.ApplyMatrixStampdp(P,dt,om);         
        end
        
        function Step(r,P,dt,t)
          r.ApplyMatrixStamp(P,dt);
        end
        
        function MicroStep(r,P,dt,t,vt)
          r.ApplyMatrixStamp(P,dt);
        end
        
        function Stepdp(r,P,dt,t,om)
            r.ApplyMatrixStamp(P,dt);
        end
        
        function PostStep(r,vsol,dt)
        end
        
        function PostStepdp(r,vsol,dt,om)
        end
        
        function ReadFlowIn(r,flow)
            if(flow(r.flin) > 0)
                r.Gc = 1/r.Ron;
            else
                r.Gc = 1/r.Roff;
            end
        end
    end
end
            
            
            
        
