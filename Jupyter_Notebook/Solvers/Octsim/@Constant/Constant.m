classdef Constant < Block
    properties
        value
    end
    methods
        function c = Constant(out,val)
            c = c@Block();
            c.value = val;
            c.noutput = 1;
            c.outpos(1) = out;
        end 
        
        function flag = ChangeParameters(c,value)
            c.value = value;
            flag =1;
        end
        
        function flag = Step(c,t,dt)
            c.y = c.value; 
            flag = 1;
        end
        
    end
end
            
            
            
        
