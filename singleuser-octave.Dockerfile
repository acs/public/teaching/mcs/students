# Copyright (c) Octave Kernel Development Team.
# Distributed under the terms of the Modified BSD License.
ARG BASE_IMAGE=jupyter/minimal-notebook
FROM $BASE_IMAGE

USER root

# Install octave
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -yq dist-upgrade \
    && apt-get install -yq --no-install-recommends \
    make \
    octave \
    liboctave-dev \
    fonts-freefont-otf \
    && rm -rf /var/lib/apt/lists/*

# Install Octave forge packages
RUN wget http://sourceforge.net/projects/octave/files/Octave%20Forge%20Packages/Individual%20Package%20Releases/control-3.2.0.tar.gz
RUN octave --eval "more off; \
                   pkg install -global -verbose \
                   control-3.2.0.tar.gz"
RUN rm control-3.2.0.tar.gz

# Install octave symbolic package
RUN octave --eval "pkg install -forge symbolic"

# Install gnuplot
RUN sudo apt-get update
RUN sudo apt-get install -y gnuplot \
    ghostscript

USER $NB_UID
	
# Install extra packages
RUN conda install --quiet --yes \
    'octave_kernel' \
    'numpy' \
    'scipy' \
    'matplotlib' \
    'sympy==1.5.1' && \
    conda clean -tipsy && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER 

# Install octave symbolic package dependency
RUN pip install 

USER $NB_UID
#USER root
#RUN echo $NB_UID
#RUN cd /
#RUN find . | grep jupyter
#RUN sudo chown -R $NB_UID:$NB_UID ~/.local/share/jupyter
#USER $NB_UID
